cmake_minimum_required(VERSION 3.5)

set(CMAKE_MODULE_PATH "${PROJECT_SOURCE_DIR}/cmake/modules" ${CMAKE_MODULE_PATH})

file(STRINGS version.txt ska_sdp_func_VERSION)
message(STATUS "Building SKA SDP processing function library, version ${ska_sdp_func_VERSION}")

# Project configuration, specifying version, languages,
# and the C++ standard to use for the whole project
project(ska_sdp_func LANGUAGES C CXX VERSION ${ska_sdp_func_VERSION})
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

# If we have Conan build information, use it to ensure that we use the
# correct packages.
if(EXISTS ${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
  include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
  conan_basic_setup()
endif()

set(CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake ${PROJECT_SOURCE_DIR}/external/cmake-modules)
include(CTest)

# Dependencies
if(${CMAKE_VERSION} VERSION_LESS "3.14.0")
  find_package(Python3 REQUIRED COMPONENTS Interpreter)
  execute_process(COMMAND ${Python3_EXECUTABLE} -c "import sys, numpy; sys.stdout.write(numpy.get_include())"
    OUTPUT_VARIABLE Python3_NumPy_INCLUDE_DIRS)
else()
  find_package(Python3 REQUIRED COMPONENTS Interpreter NumPy)
endif()
find_package(HDF5 REQUIRED COMPONENTS C)
find_package(GTest REQUIRED)
find_package(PkgConfig REQUIRED)
find_package(ZLIB REQUIRED) # Not entirely sure why, but needed for linking successfully

pkg_search_module(FFTW REQUIRED fftw3 IMPORTED_TARGET)

# Find (Py)Arrow. In the process we are going to be looking for
# gRPC+re2, which produces noisy and unhelpful warnings when the
# libraries don't provide their own CMake configurations (which
# apparently they basically never do), so we silence them.
option(PYTHON "Build Python package" ON)
option(ARROW_SHARED "Use Apache Arrow as shared library" ON)
option(ARROW_PKGCONFIG "Use PkgConfig to find Arrow" OFF)
set(gRPCAlt_FIND_QUIETLY ON)
set(re2Alt_FIND_QUIETLY ON)
set(c-aresAlt_FIND_QUIETLY ON)
if (${PYTHON})
  # Python binding dependencies
  set(CMAKE_POSITION_INDEPENDENT_CODE ON)
  execute_process(COMMAND ${Python3_EXECUTABLE} -c "import sys, pybind11; sys.stdout.write(pybind11.get_cmake_dir())"
    OUTPUT_VARIABLE PyBind11_CMAKE_DIRS)
  list(APPEND CMAKE_PREFIX_PATH ${PyBind11_CMAKE_DIRS})
  find_package(pybind11 REQUIRED)
  find_package(Pyarrow REQUIRED)
endif()

if(NOT DEFINED ARROW_LIBRARIES)
  if(CONAN_LIB_DIRS_ARROW)
    find_library(ARROW_LIBRARIES arrow PATHS ${CONAN_LIB_DIRS_ARROW})
  elseif(ARROW_PKGCONFIG)
    pkg_search_module(Arrow REQUIRED arrow IMPORTED_TARGET)
    set(ARROW_LIBRARIES PkgConfig::Arrow)
  else()
    find_package(Arrow REQUIRED)
    if (ARROW_SHARED)
      set(ARROW_LIBRARIES arrow_shared)
    else()
      set(ARROW_LIBRARIES arrow_static)
    endif()
  endif()
endif()
message(STATUS "Arrow: ${ARROW_LIBRARIES}")

if(PYTHON)
  if(NOT DEFINED PYARROW_LIBRARIES)
    if(ARROW_PKGCONFIG)
      pkg_search_module(PyArrow REQUIRED arrow-python IMPORTED_TARGET)
      set(PYARROW_LIBRARIES PkgConfig::PyArrow)
    else()
      set(PYARROW_LIBRARIES arrow_python)
    endif()
  endif()
  message(STATUS "PyArrow: ${PYARROW_LIBRARIES}")
endif()

# Default compilation options
add_compile_options(-Wall -ffast-math -O2)

# Coverage options
option(COVERAGE "Enable Code Coverage" OFF)
if (${COVERAGE})
  add_compile_options(-ftest-coverage -fprofile-arcs)
  add_link_options(-coverage)
endif()

add_subdirectory(src/ska-sdp-func)
if (${PYTHON})
  add_subdirectory(src/ska-sdp-func-python)
endif()
add_subdirectory(src/apps)
add_subdirectory(docs)

# Install cmake config + version + target files
include(CMakePackageConfigHelpers)
configure_package_config_file(
  cmake/ska-sdp-func-config.cmake.in
  "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/ska-sdp-func-config.cmake"
  INSTALL_DESTINATION
    share/ska-sdp-func/cmake
)
write_basic_package_version_file(
  "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/ska-sdp-func-version.cmake"
  COMPATIBILITY
    AnyNewerVersion
)
install(FILES
  ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/ska-sdp-func-config.cmake
  ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/ska-sdp-func-version.cmake
  DESTINATION
    share/ska-sdp-func/cmake
  COMPONENT
    dev
)
install(EXPORT ska-sdp-func-targets
  FILE
    ska-sdp-func-targets.cmake
  DESTINATION
    share/ska-sdp-func/cmake
  COMPONENT
    dev
  NAMESPACE
    ska_sdp_func::
)

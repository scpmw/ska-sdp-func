from conans import ConanFile, CMake, tools


class SkaSdpFuncConan(ConanFile):
    name = "ska-sdp-func"
    version = "0.0.1"
    license = "BSD"
    author = "Peter Wortmann <Peter.Wortmann@skao.int>"
    url = "https://gitlab.com/ska-telescope/sdp/ska-sdp-func-iotest"
    description = "SKA SDP processing function library (I/O test)"
    topics = ()
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [False, True]}
    default_options = {"shared": False}
    generators = "cmake"
    exports_sources = [
        "cmake/*.cmake", "cmake/*.cmake.in",
        "src/*.cc", "src/*.h", "src/*.h.in", "src/*.cmake.in", "src/*.py",
        "src/*/CMakeLists.txt",
        "docs/*.py", "docs/*.md", "docs/CMakeLists.txt",
        "data/*.h5",
        "CMakeLists.txt", "version.txt"
    ]
    requires = [
        "fftw/3.3.9",
        "hdf5/1.12.0",
        "arrow/2.0.0",
        "gtest/1.11.0",
        "zlib/1.2.11"
    ]

    def configure(self):
        self.options["arrow"].shared = self.options.shared
        self.options["hdf5"].shared = self.options.shared
        self.options["fftw"].shared = self.options.shared
        self.options["gtest"].shared = self.options.shared
        self.options["zlib"].shared = self.options.shared
        pass

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC
        
    def build(self):
        cmake = CMake(self)
        if self.should_configure:
            cmake.configure(defs={
                "PYTHON": False
            })
        if self.should_build:
            cmake.build()
        if self.should_test:
            cmake.test()

    def package(self):
        cmake = CMake(self)
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["ska-sdp-func"]


#include <iostream>

#include "ska-sdp-func/config.h"

int main() {

	std::cout << "Hello world version: " << SKA_SDP_FUNC_VERSION_MAJOR << ":"
	          << SKA_SDP_FUNC_VERSION_MINOR << ":"
	          << SKA_SDP_FUNC_VERSION_PATCH <<  std::endl;

	return 0;
}

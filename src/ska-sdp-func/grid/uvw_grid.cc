
#include "uvw_grid.h"

#include <assert.h>
#include <float.h>
#include <complex.h>
#include <stdint.h>
#include <fenv.h>
#include <math.h>
#include <stdio.h>
#include <immintrin.h>

namespace ska_sdp_func {

constexpr size_t ALIGN = 32;

void *a_alloc(const size_t size, const size_t align)
{
    return aligned_alloc(align, ((size + align-1) / align) * align);
}

uvw_grid::uvw_grid(const double du, const double dv, const double dw,
                   const tensor_ptr<double> &kernel,
                   const tensor_ptr<double> &corr,
                   const tensor_ptr<double> &wkernel,
                   const tensor_ptr<double> &wcorr)
    : du(du), dv(dv), dw(dw)
{

    const double *pkernel, *pcorr, *pwkernel, *pwcorr;
    tensor_dim_info<double> kernel_x, kernel_ov, corr_x, wkernel_x, wkernel_ov, wcorr_x;
    std::tie(pkernel, kernel_ov, kernel_x) = tensor_unpack(
        "uvw_grid::uvw_grid", "kernel", kernel, dim::ov, dim::u);
    std::tie(pcorr, corr_x) = tensor_unpack(
        "uvw_grid::uvw_grid", "corr", corr, dim::l);
    std::tie(pwkernel, wkernel_ov, wkernel_x) = tensor_unpack(
        "uvw_grid::uvw_grid", "wkernel", wkernel, dim::ov, dim::u);
    std::tie(pwcorr, wcorr_x) = tensor_unpack(
        "uvw_grid::uvw_grid", "wcorr", wcorr, dim::l);

    // Copy u/v kernel data
    const int stride_align = (ALIGN + sizeof(double) - 1) / sizeof(double);
    this->kernel.size = kernel_x.size;
    this->kernel.stride = ((kernel_x.size + stride_align - 1) / stride_align) * stride_align;
    this->kernel.oversampling = kernel_ov.size;
    this->kernel.corr_size = corr_x.size;
    this->kernel.data = reinterpret_cast<double *>(a_alloc(sizeof(double) * kernel_ov.size * this->kernel.stride, ALIGN));
    this->kernel.corr = reinterpret_cast<double *>(a_alloc(sizeof(double) * corr_x.size, ALIGN));
    for (int i = 0; i < kernel_ov.size; i++) {
        for (int j = 0; j < kernel_x.size; j++) {
            this->kernel.data[i * this->kernel.stride + j] =
                pkernel[i * kernel_ov.stride + j * kernel_x.stride];
        }
    }
    for (int i = 0; i < corr_x.size; i++) {
        this->kernel.corr[i] = pcorr[i * corr_x.stride];
    }

    // Copy w kernel data
    this->wkernel.size = wkernel_x.size;
    this->wkernel.stride = ((wkernel_x.size + stride_align - 1) / stride_align) * stride_align;
    this->wkernel.oversampling = wkernel_ov.size;
    this->wkernel.corr_size = wcorr_x.size;
    this->wkernel.data = reinterpret_cast<double *>(a_alloc(sizeof(double) * wkernel_ov.size * this->wkernel.stride, ALIGN));
    this->wkernel.corr = reinterpret_cast<double *>(a_alloc(sizeof(double) * wcorr_x.size, ALIGN));
    for (int i = 0; i < wkernel_ov.size; i++) {
        for (int j = 0; j < wkernel_x.size; j++) {
            this->wkernel.data[i * this->wkernel.stride + j] =
                pwkernel[i * wkernel_ov.stride + j * wkernel_x.stride];
        }
    }
    for (int i = 0; i < wcorr_x.size; i++) {
        this->wkernel.corr[i] = pwcorr[i * wcorr_x.stride];
    }

}

uvw_grid::~uvw_grid()
{
    free(kernel.data);
    free(kernel.corr);
    free(wkernel.data);
    free(wkernel.corr);
}

inline static void _frac_coord(const int64_t oversample, const double u, int64_t *const x, int64_t *const fx)
{

    assert(u >= 0);

    // Round to nearest oversampling value. We assume the kernel to be
    // in "natural" order, so what we are looking for is the best
    //    "x - fx / oversample"
    // approximation for u. Note that taken together we need about 35
    // significant bits here to deal with fine oversampling and large
    // grids. Really need double precision!
    const double o = u * oversample;
#ifdef __SSE4_1__
    __m128d oxd = _mm_round_pd(_mm_set_pd(0, o),_MM_FROUND_TO_NEAREST_INT);
    int64_t ox = (int64_t) oxd[0];
#else
    fesetround(3); int64_t ox = lrint(o);
#endif
    ox += oversample - 1;
    *x = ox / oversample;
    *fx = oversample - 1 - (ox % oversample);

}

#ifdef __SSE4_1__
#define SSE_ALIGN 16
inline static void
_frac_coord_uv(const int64_t oversample,
               const double u, const double v, int64_t *const px, int64_t *const pfx)
{

    // Round to nearest oversampling value. We assume the kernel to be
    // in "natural" order, so what we are looking for is the best
    //    "x - fx / oversample"
    // approximation for "grid_size/2 + u".
    __m128d ov = _mm_set_pd(oversample, oversample);
    __m128d o = _mm_set_pd(v, u);
    __m128d ox = _mm_round_pd(o * ov, _MM_FROUND_TO_NEAREST_INT | _MM_FROUND_NO_EXC);
    __m128d x = _mm_round_pd(ox / ov, _MM_FROUND_TO_POS_INF | _MM_FROUND_NO_EXC);
    __m128i ix = _mm_cvtpd_epi32(x);

    __attribute__((aligned(SSE_ALIGN))) int32_t ix_out[4];
    _mm_store_si128((__m128i *)ix_out, ix);
    px[0] = ix_out[0];
    px[1] = ix_out[1];

    __attribute__((aligned(SSE_ALIGN))) int32_t ifx_out[4];
    _mm_store_si128((__m128i *)ifx_out, _mm_cvtpd_epi32(ov * x - ox));
    pfx[0] = ifx_out[0];
    pfx[1] = ifx_out[1];

}
#endif

/**
 * \brief Fractional coordinate separation from u,v,w to grid_offset, sub_offset_x, sub_offset_y, sub_offset_z.
 */
inline static void
frac_coord_sep_uvw(const int64_t x_stride, const int64_t y_stride,
                   const int64_t kernel_size, const int64_t kernel_stride, const int64_t over,
                   const int64_t wkernel_size, const int64_t wkernel_stride, const int64_t over_w,
                   const double u, const double v, const double w,
                   int64_t *const grid_offset, int64_t *const plane_offset,
                   int64_t *const sub_offset_x, int64_t *const sub_offset_y, int64_t *const sub_offset_z)
{
    // Find fractional coordinates
    int64_t ix[2], iz, ixf[2], izf; // NOLINT
#ifndef __SSE4_1__
    _frac_coord(over, u, &ix[0], &ixf[0]);
    _frac_coord(over, v, &ix[1], &ixf[1]);
#else
    _frac_coord_uv(over, u, v, ix, ixf);
#endif
    _frac_coord(over_w, w, &iz, &izf);

    // Calculate grid and oversampled kernel offsets
    *grid_offset = (ix[1]-kernel_size/2)*y_stride + (ix[0]-kernel_size/2)*x_stride;
    if (plane_offset) *plane_offset = iz-wkernel_size/2;
    *sub_offset_x = kernel_stride * ixf[0];
    *sub_offset_y = kernel_stride * ixf[1];
    *sub_offset_z = wkernel_stride * izf;
}

#ifdef __GNUC__
static const bool HAVE_FMA = __builtin_cpu_supports("fma");
static const bool HAVE_AVX2 = __builtin_cpu_supports("avx2");
#elif defined(__INTEL_COMPILER)
static const bool HAVE_FMA = _may_i_use_cpu_feature(_FEATURE_FMA);
static const bool HAVE_AVX2 = _may_i_use_cpu_feature(_FEATURE_AVX2);
#else
#error Unsure how to detect processor features for this compiler!
#endif

static inline void
degrid_line (const int64_t ch0, const int64_t end_ch,
             const int64_t w_size,
             const bool conjugate,
             const double u0, const double v0, const double w0,
             const double du_g, const double dv_g, const double dw_g,
             double const*const kernel, const int64_t kernel_size,
             const int64_t kernel_oversampling, const int64_t kernel_stride,
             double const*const wkernel, const int64_t wkernel_size,
             const int64_t wkernel_oversampling, const int64_t wkernel_stride,
             complex<double> *const pv0, const int64_t vis_ch_stride,
             complex<double> const*const grid,
             const int64_t grid_u_stride, const int64_t grid_v_stride,
             const int64_t grid_w_stride, const int64_t grid_w_roll)
{
    const double im_mult = (conjugate ? -1 : 1);

    // Include specialised versions - will "return" if any apply.
    #include "griduvw2_all.h"

    // Fall back to reference version
    double u = u0, v = v0, w = w0; int64_t ch = ch0; complex<double> *pv = pv0;
    for (; ch < end_ch; ch++, u+=du_g, v+=dv_g, w+=dw_g, pv+=vis_ch_stride) {

        // Calculate fractional coordinates
        int64_t grid_offset=0, plane_offset=0, sub_offset_x=0, sub_offset_y=0, sub_offset_z=0; // NOLINT
        frac_coord_sep_uvw(grid_u_stride, grid_v_stride,
                           kernel_size, kernel_stride, kernel_oversampling,
                           wkernel_size, wkernel_stride, wkernel_oversampling,
                           u, v, w, &grid_offset, &plane_offset,
                           &sub_offset_x, &sub_offset_y, &sub_offset_z);

        // Get visibility by deconvolution along w,v,u axes
        complex<double> vis = 0;
        for (int64_t z = 0; z < wkernel_size; z++) {
            complex<double> visz = 0;
            const complex<double> *uvgrid = grid + ((z+plane_offset+grid_w_roll) % w_size) * grid_w_stride;
            for (int64_t y = 0; y < kernel_size; y++) {
                complex<double> visy = 0;
                for (int64_t x = 0; x < kernel_size; x++) {
                    visy += kernel[sub_offset_x + x] *
                        uvgrid[grid_offset + y*grid_v_stride + x*grid_u_stride];
                }
                visz += kernel[sub_offset_y + y] * visy;
            }
            vis += wkernel[sub_offset_z + z] * visz;
        }
        vis = std::complex<double>(real(vis), imag(vis) * im_mult);
        *pv = vis;
    }

}

void uvw_grid::degrid(
    const int64_t u0, const int64_t v0, const int64_t w0,
    const int64_t ch0, const bool conjugate,
    const tensor_ptr<double> &uvw_ld,
    const tensor_ptr<int64_t> &channels,
    const tensor_ptr<complex<double> > &grid, const int64_t grid_w_roll,
    const tensor_ptr<complex<double> > &vis_out) const
{

    // Check memory space, size and stride assumptions
    const double *puvw_ld; tensor_dim_info<double> uvw_step, uvw_lds;
    std::tie(puvw_ld, uvw_step, uvw_lds) = tensor_unpack(
        "uvw_grid::degrid", "uvw_ld", uvw_ld, dim::step, dim::ld);
    const int64_t *pchannels; tensor_dim_info<int64_t> ch_step, ch_chs;
    std::tie(pchannels, ch_step, ch_chs) = tensor_unpack(
        "uvw_grid::degrid", "channels", channels, dim::step, dim::range);
    ch_step.check_equal_size(uvw_step, true);
    const complex<double> *pgrid; tensor_dim_info<complex<double> > u_dim, v_dim, w_dim;
    std::tie(pgrid, w_dim, v_dim, u_dim) = tensor_unpack_cpx(
        "uvw_grid::degrid", "grid", grid, dim::w, dim::v, dim::u);
    complex<double> *pvis_out;tensor_dim_info<complex<double> > vis_step, vis_ch;
    std::tie(pvis_out, vis_step, vis_ch) = tensor_unpack_mutable_cpx(
        "uvw_grid::degrid", "vis_out", vis_out, dim::step, dim::ch);
    vis_step.check_equal_size(uvw_step, false);
    const double grid_du = this->du;
    const double grid_dv = this->dv;
    const double grid_dw = this->dw;

    // Go through steps
    for (int64_t step=0; step < vis_step.size; step++) {

        // Get channels
        int64_t start_ch = pchannels[ch_step(step)+ch_chs(0)];
        int64_t end_ch = pchannels[ch_step(step)+ch_chs(1)];
        if (start_ch < ch0) start_ch = ch0;
        if (end_ch > ch0+vis_ch.size) end_ch = ch0+vis_ch.size;
        if (start_ch >= end_ch) start_ch = ch0+vis_ch.size;

        // Fill start with zeroes
        complex<double> *pv = pvis_out + vis_step(step);
        int64_t ch = ch0;
        for (; ch < start_ch; ch++, pv+=vis_ch.stride) {
            *pv = 0;
        }
        if (ch > end_ch) {
            continue;
        }

        // Calculate start and end of visibility line - and make
        // extra-sure it actually sits within our set bounds.
        const double *uvw0 = puvw_ld + uvw_step(step);
        const double du = uvw0[uvw_lds(3)], dv = uvw0[uvw_lds(4)], dw = uvw0[uvw_lds(5)];
        double u = (uvw0[uvw_lds(0)]+start_ch*du)/grid_du - u0;
        double v = (uvw0[uvw_lds(1)]+start_ch*dv)/grid_dv - v0;
        double w = (uvw0[uvw_lds(2)]+start_ch*dw)/grid_dw - w0;
#ifndef NDEBUG
        int64_t nch = end_ch - start_ch - 1;
        double u1 = u + du/grid_du * nch;
        double v1 = v + dv/grid_dv * nch;
        double w1 = w + dw/grid_dw * nch;
        const double eps = 1e-3;
        if (!(u - kernel.size / 2 >= 0 && u + kernel.size / 2 < u_dim.size &&
              u1 - kernel.size / 2 >= 0 && u1 + kernel.size / 2 < u_dim.size &&
              v - kernel.size / 2 >= 0 && v + kernel.size / 2 < v_dim.size &&
              v1 - kernel.size / 2 >= 0 && v1 + kernel.size / 2 < v_dim.size &&
              w - wkernel.size / 2 >= 0 && w + wkernel.size / 2 - eps < w_dim.size &&
              w1 - wkernel.size / 2 >= 0 && w1 + wkernel.size / 2 - eps < w_dim.size))
            throw parameter_error(
                "uvw_grid::degrid", "channels",
                "Step ", step, " (channels ", start_ch, "-", end_ch, ") covers uvw (",
                u0+u, ",", v0+v, ",", w0+w, ")-(", u0+u1, ",", v0+v1, ",", w0+w1,
                "), which is outside the expected uvw range (",
                u0+kernel.size/2, ":", u0+u_dim.size-kernel.size/2, "(", kernel.oversampling, " ", kernel.size, "),",
                v0+kernel.size/2, ":", v0+v_dim.size-kernel.size/2, ",",
                w0+wkernel.size/2, ":", w0+w_dim.size-wkernel.size/2, ")");
#endif

        // Degrid a line of visibilities (this is the main worker here)
        degrid_line(ch, end_ch, w_dim.size, conjugate,
                    u, v, w, du/grid_du, dv/grid_dv, dw/grid_dw,
                    kernel.data, kernel.size, kernel.oversampling, kernel.stride,
                    wkernel.data, wkernel.size, wkernel.oversampling, wkernel.stride,
                    pv, vis_ch.stride, pgrid, u_dim.stride, v_dim.stride, w_dim.stride, grid_w_roll);

        // Fill rest with zeroes
        for (ch = end_ch; ch < ch0+vis_ch.size; ch++) {
            pvis_out[vis_step(step) + vis_ch(ch-ch0)] = 0;
        }

    }

}

static double lookup_log_inter(double const*const data, const int64_t size, double ix)
{
    ix += size;
    const double f_ix = floor(ix);
    double v0 = data[((int64_t)f_ix) % size];
    double v1 = data[(((int64_t)f_ix)+1) % size];
    const double w = ix - f_ix;
    if (v0 == 0) v0 = 1e-15;
    if (v1 == 0) v1 = 1e-15;
    return exp((1-w) * log(v0) + w * log(v1));
}

void uvw_grid::degrid_correct(
    const int64_t il0, const int64_t im0,
    const double l_mid, const double dl,
    const double m_mid, const double dm,
    const tensor_ptr<complex<double> > &image_inout) const
{

    complex<double> *pimage; tensor_dim_info<complex<double> > l_dim, m_dim;
    std::tie(pimage, m_dim, l_dim) = tensor_unpack_mutable_cpx(
        "uvw_grid::degrid_correct", "image_inout", image_inout, dim::m, dim::l);

    // Bring kernel data into stack context
    const struct sep_kernel_data kernel = this->kernel;
    const struct sep_kernel_data wkernel = this->wkernel;

    // Ensure that coordinates are not larger than the image. Strictly
    // speaking we likely want them to be substantially smaller, as
    // gridding will not be precise all the way to the borders of the image.
    const double cl_min = l_mid + dl * il0;
    const double cl_max = l_mid + dl * (il0 + l_dim.size - 1);
    if (cl_min * du < -0.5 || cl_max * du > 0.5)
        throw parameter_error("uvw_grid::degrid_correct", "l_mid",
                              "Querying grid correction for l range [", cl_min, ";", cl_max,
                              "], which is outside the permissable [", -1/du/2, ";", 1/du/2,
                              "] range given by du=", du, "!");
    const double cm_min = m_mid + dm * im0;
    const double cm_max = m_mid + dm * (im0 + m_dim.size - 1);
    if (cm_min * dv < -0.5 || cm_max * dv > 0.5)
        throw parameter_error("uvw_grid::degrid_correct", "m_mid",
                              "Querying grid correction for m range [", cm_min, ";", cm_max,
                              "], which is outside the permissable [", -1/dv/2, ";", 1/dv/2,
                              "] range given by du=", dv, "!");

    // Apply grid correction
    // TODOs:
    // * Optimise for cases where resolution matches
    // * Cache correction values for l-direction so we don't need to re-compute per row
    for (int m = 0; m < m_dim.size; m++) {

        // Calculate directional cosine for m-axis, look up correction
        const double cm = m_mid + dm * (im0 + m);
        const double corr_m = lookup_log_inter(kernel.corr, kernel.corr_size,
                                               cm * kernel.corr_size * dv);
        for (int l = 0; l < l_dim.size; l++) {

            // Calculate directional cosine for l-axis, look up correction
            const double cl = (l_mid + dl * (il0 + l));
            const double corr_l = lookup_log_inter(kernel.corr, kernel.corr_size,
                                                   cl * kernel.corr_size * du);
            // Calculate direction cosine for n-axis, look up correction
            const double cn = sqrt(1 - cm*cm - cl*cl) - 1;
            const double corr_n = lookup_log_inter(wkernel.corr, wkernel.corr_size,
                                                   cn * wkernel.corr_size * dw);
            // Apply correction
            pimage[l_dim(l) + m_dim(m)] /= corr_m * corr_l * corr_n;
        }
    }

}

} // namespace ska_sdp_func

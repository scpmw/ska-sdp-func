#ifndef SKA_SPD_FUNC_GRID_UVW_GRID_H
#define SKA_SPD_FUNC_GRID_UVW_GRID_H

#include "ska-sdp-func/internal/tensor.h"

namespace ska_sdp_func {

/** 3D gridder processing component
 *
 * This processing component provides two main functions: (De)gridding of
 * visibilities from a 3D uvw grid using oversampled gridding functions, and
 * the respective grid correction.
 */
class uvw_grid final {
public:

    /**
     * Initialise UVW gridder
     *
     * Selects and prepares the kernels to use
     *
     * @param du  Grid resolution along u axis
     * @param dv  Grid resolution along v axis
     * @param dw  Grid resolution along w axis
     * @param kernel `[ov][x]` u/v kernel data
     * @param corr `[x]` u/v kernel correction data
     * @param wkernel `[ov][x]` w kernel data
     * @param wcorr `[x]` w kernel correction data
     */
    uvw_grid(double du, double dv, double dw,
	     const tensor_ptr<double> &kernel,
             const tensor_ptr<double> &corr,
             const tensor_ptr<double> &wkernel,
             const tensor_ptr<double> &wcorr);

    ~uvw_grid();

    /** \verbatim embed:rst:leading-asterisk
     * Perform gridding correction to the given image
     *
     * This means dividing the image by the grid correction. After FFT, we can use
     * uvw_grid_degrid() to obtain visibilities.
     *
     * @param il0  Index of first coordinate on l axis
     * @param im0  Index of first coordinate on l axis
     * @param l_mid  Horizontal directional cosine of image midpoint
     * @param dl  Horizontal resolution
     * @param m_mid  Vertical directional cosine of image midpoint
     * @param dl  Vertical resolution
     * @param image_inout  Image data
     * \endverbatim
     */
    void
    degrid_correct(int64_t il0, int64_t im0,
                   double l_mid, double dl,
                   double m_mid, double dm,
                   const tensor_ptr<double> &image_inout) const;


    /**
     * Degrid visibilities using oversampled separable 3D kernels.
     *
     * Should ensure that visibilities are assigned uniquely, i.e. that if we pass
     *
     * @param u0  Index of first coordinate on u axis
     * @param v0  Index of first coordinate on v axis
     * @param w0  Index of first coordinate on w axis
     * @param ch0  Index of first channel
     * @param conjugate  Whether to generate conjugated visibilities
     * @param uvw_ld  `double[step][ld=6:1]` Uvw coordinates per step, in `wavelengths + delta form`
     * @param channels  `int[step][chs=2:1]` Channel ranges, given as pairs `(start, end)` per step.
     * @param grid  `complex<double>[w][v][u]` Input grid data
     * @param vis_out  `complex<double>[step][ch]` Output visibilities
     */
    void
    degrid(const int64_t u0, const int64_t v0, const int64_t w0, const int64_t ch0, const bool conjugate,
           const tensor_ptr<double> &uvw_ld,
           const tensor_ptr<int64_t> &channels,
           const tensor_ptr<double> &grid, int64_t grid_w_roll,
           const tensor_ptr<double> &vis_out) const;

private:

   /**
    * \struct sep_kernel_data
    * \brief Separable kernel data (with correction).
    */
    struct sep_kernel_data {
	/// grid-plane kernel
	int64_t size;
	int64_t stride;
	int64_t oversampling;
	double *data; // [oversampling][stride]
	// image-plane correction
	int64_t corr_size;
	double *corr; // [corr_size]
    };

    const double du, dv, dw;
    struct sep_kernel_data kernel;
    struct sep_kernel_data wkernel;

};

} // namespace

#endif // SKA_SPD_FUNC_GRID_UVW_GRID_H

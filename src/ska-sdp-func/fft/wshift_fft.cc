
#include "wshift_fft.h"

#include <fftw3.h>
#include <iostream>

const int64_t FFT_ALIGN = 16;

namespace ska_sdp_func {

wshift_fft::wshift_fft(const double du, const double dv, const double dw,
                       const tensor_ptr<double> &plan_area)
{

    // Get + check plan
    complex<double> *pplan_area;
    std::tie(pplan_area, plan_v, plan_u) = tensor_unpack_mutable_cpx(
        "wshift_fft::wshift_fft", "plan_area", plan_area, dim::v, dim::u);
    if (plan_u.size % 2 != 0 || plan_v.size % 2 != 0)
        throw parameter_error("wshift_fft", "plan_area",
                              "Size is ", plan_u.size, "x", plan_v.size, ", but must be even!");

    // Allocate & calculate w-transfer pattern
    wtransfer = reinterpret_cast<complex<double> *>(
        a_alloc(plan_u.size * plan_v.size * sizeof(complex<double>), sizeof(complex<double>)));
    for (int64_t im = 0; im < plan_v.size; im++) {
        for (int64_t il = 0; il < plan_u.size; il++) {
            const double l = double(il - plan_u.size/2) / plan_u.size / du;
            const double m = double(im - plan_v.size/2) / plan_v.size / dv;
            const double n = sqrt(1 - l*l - m*m) - 1;
            const double ph = -2 * M_PI * dw * n;
            wtransfer[im*plan_u.size+il] = complex<double>(cos(ph), sin(ph));
        }
    }

    // Plan (inplace) FFT, as that is what we are going to do later
    const fftw_iodim dims[2] = {
        { .n = int(plan_u.size), .is = int(plan_u.stride), .os = int(plan_u.stride) },
        { .n = int(plan_v.size), .is = int(plan_v.stride), .os = int(plan_v.stride) }
    };
    fftw_complex *plan_area_fftw = reinterpret_cast<fftw_complex*>(pplan_area);
    plan = fftw_plan_guru_dft(2, dims, 0, NULL, plan_area_fftw, plan_area_fftw,
                              FFTW_FORWARD, FFTW_MEASURE);
}

wshift_fft::~wshift_fft()
{
    free(wtransfer);
    fftw_free(plan);
}

// (stolen from Stack Overflow)
inline static complex<double> cipow(complex<double> base, int exp)
{
    complex<double> result = 1;
    if (exp < 0) return 1. / cipow(base, -exp);
    if (exp == 1) return base;
    while (exp)
    {
        if (exp & 1)
            result *= base;
        exp >>= 1;
        base *= base;
    }
    return result;
}

void wshift_fft::fft(const int64_t w0,
                     const tensor_ptr<double> &image,
                     const tensor_ptr<double> &grid_out) const
{

    // Get image + grid data and strides
    const complex<double> *image_data; tensor_dim_info<complex<double> > image_l, image_m, image_bc;
    std::tie(image_data, image_bc, image_m, image_l) = tensor_unpack_cpx(
        "wshift_fft::fft", "image", image, dim::bc, dim::m, dim::l);
    image_l.check_size(plan_u.size);
    image_m.check_size(plan_v.size);
    complex<double> *grid_data; tensor_dim_info<complex<double> > grid_u, grid_v, grid_w, grid_bc;
    std::tie(grid_data, grid_bc, grid_w, grid_v, grid_u) = tensor_unpack_mutable_cpx(
        "wshift_fft::fft", "grid_out", grid_out, dim::bc, dim::w, dim::v, dim::u);
    image_bc.check_equal_size(grid_bc, true);
    grid_v.check_equal_size_stride(plan_v);
    grid_u.check_equal_size_stride(plan_u);

    // Shift + copy + multiply + apply shift pattern
    // TODO: Specialise for wstep values...
    const int64_t l_size_2 = plan_u.size / 2;
    const int64_t m_size_2 = plan_v.size / 2;
    for (int64_t bc = 0; bc < image_bc.size; bc++) {
        for (int64_t m = 0; m < plan_v.size; m++) {
            for (int64_t l = 0; l < plan_u.size; l++) {
                const int64_t l2 = (l+l_size_2) % grid_u.size;
                const int64_t m2 = (m+m_size_2) % grid_v.size;
                const double shift_mult = 1 - 2 * ((m+l) % 2);
                const complex<double> wtrans = wtransfer[l2+m2*plan_u.size];
                complex<double> val = shift_mult * cipow(wtrans, w0) *
                    image_data[image_bc(bc) + image_l(l2) + image_m(m2)];
                for (int64_t w = 0; w < grid_w.size; w++) {
                    grid_data[grid_bc(bc) + grid_w(w) + grid_u(l) + grid_v(m)] = val;
                    val *= wtrans;
                }
            }
        }

        // Perform FFT(s)
        for (int64_t w = 0; w < grid_w.size; w++) {
            fftw_complex *fftw_grid_data = reinterpret_cast<fftw_complex *>(
                grid_data + grid_bc(bc) + grid_w(w));
            fftw_execute_dft(plan, fftw_grid_data, fftw_grid_data);
        }
    }

}

} // namespace ska_sdp_func

#ifndef SKA_SPD_FUNC_FFT_WSHIFT_FFT_H
#define SKA_SPD_FUNC_FFT_WSHIFT_FFT_H

#include "ska-sdp-func/internal/tensor.h"

#include <fftw3.h>

namespace ska_sdp_func {

/** Fourier Transform with w-shift processing component */
class wshift_fft final {
public:

    wshift_fft(double du, double dv, double dw,
               const tensor_ptr<double> &plan_area);
    ~wshift_fft();

    void fft(int64_t wstep,
             const tensor_ptr<double> &image,
             const tensor_ptr<double> &grid_out) const;

private:
    struct tensor_dim_info<complex<double> > plan_u, plan_v;
    complex<double> *wtransfer;
    fftw_plan plan;

};

} // namespace

#endif // SKA_SPD_FUNC_FFT_WSHIFT_FFT_H

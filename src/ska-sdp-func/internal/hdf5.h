
#pragma once

#include <hdf5.h>

namespace ska_sdp_func {

/**
 * Map Arrow type to HDF5 type
 * @param
 */
inline arrow::Result<hid_t> arrow_type_to_hdf5(arrow::Type::type typ) {
    // Likely not complete
    switch(typ) {
    case arrow::Type::type::BOOL: return H5T_NATIVE_HBOOL;
    case arrow::Type::type::UINT8: return H5T_STD_U8LE;
    case arrow::Type::type::INT8: return H5T_STD_I8LE;
    case arrow::Type::type::UINT16: return H5T_STD_U16LE;
    case arrow::Type::type::INT16: return H5T_STD_I16LE;
    case arrow::Type::type::UINT32: return H5T_STD_U32LE;
    case arrow::Type::type::INT32: return H5T_STD_I32LE;
    case arrow::Type::type::UINT64: return H5T_STD_U64LE;
    case arrow::Type::type::INT64: return H5T_STD_I64LE;
    case arrow::Type::type::FLOAT: return H5T_IEEE_F32LE;
    case arrow::Type::type::DOUBLE: return H5T_IEEE_F64LE;
    default: break;
    }
    return arrow::Status::TypeError("Unsupported Arrow type: %d", typ);
}

/**
 * \brief Read HDF5 data from a file.
 * \param typ         Number type to read
 * \param is_complex  Complex numbers (i.e. real+imaginary pairs)?
 * \param dim_names   Dimension names
 * \param file        HDF5 file to read
 * \param name        HDF5 dataset to access
 * \return void* pointer to the data that was read.
 */
template <typename TYPE>
arrow::Result<std::shared_ptr<arrow::NumericTensor<TYPE> > >
read_tensor_from_hdf5(const TYPE &typ, bool is_complex,
		      const std::vector<std::string> &dim_names,
		      const std::string &file, const std::string &name)
{
    // Open the file
    hid_t f = H5Fopen(file.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
    if (f < 0) {
	return arrow::Status::IOError("Could not open HDF5 file", file, "!");
    }

    // Determine name of dataset to read, open it
    hid_t dset = H5Dopen(f, name.c_str(), H5P_DEFAULT);

    // Check data type
    ARROW_ASSIGN_OR_RAISE(hid_t type_id, arrow_type_to_hdf5(typ.id()));
    if (is_complex) {
	hid_t cpx_id = H5Tcreate(H5T_COMPOUND, 2 * H5Tget_size(type_id));
	H5Tinsert(cpx_id, "r", 0, type_id);
	H5Tinsert(cpx_id, "i", H5Tget_size(type_id), type_id);
	type_id = cpx_id;
    }
    if (H5Tequal(H5Dget_type(dset), type_id) <= 0) {
	H5Dclose(dset); H5Fclose(f);
	return arrow::Status::TypeError("HDF5 dataset '", name, "' in '", file, "' has wrong type!");
    }

    // Check number of dimensions
    hid_t space = H5Dget_space(dset);
    int ndims = H5Sget_simple_extent_ndims(space);
    if (ndims < 0 || static_cast<size_t>(ndims) != dim_names.size()) {
	H5Dclose(dset); H5Fclose(f);
	return arrow::Status::TypeError(
	    "HDF5 dataset '",name,"' in '",file,
	    "' has wrong number of dimensions: expected ",
	    dim_names.size(), ", found ", ndims, "!");
    }

    // Allocate vectors for dimensions
    std::vector<hsize_t> dims(ndims);
    if (H5Sget_simple_extent_dims(space, &dims[0], NULL) < 0) {
	H5Dclose(dset); H5Fclose(f);
	return arrow::Status::TypeError(
	    "Could not query dimensions of HDF5 dataset '", name, "'!");
    }
    std::vector<int64_t> dims_arrow;
    std::copy(dims.begin(), dims.end(), std::back_inserter(dims_arrow));
    if (is_complex) {
	dims_arrow.push_back(2);
    }

    // Allocate buffer
    int npoints = H5Sget_simple_extent_npoints(space);
    ARROW_ASSIGN_OR_RAISE(std::shared_ptr<arrow::Buffer> buf,
			  arrow::AllocateBuffer(typ.bit_width() * npoints / CHAR_BIT));

    // Read data
    H5Dread(dset, H5Dget_type(dset), H5S_ALL, H5S_ALL, H5P_DEFAULT,
	    buf->mutable_data());
    H5Dclose(dset); H5Fclose(f);
    if (is_complex) {

	// Add "cpx" to dimension names
	std::vector<std::string> dim_names_cpx;
	std::copy(dim_names.begin(), dim_names.end(), std::back_inserter(dim_names_cpx));
	dim_names_cpx.push_back("cpx");

	return arrow::NumericTensor<TYPE>::Make(buf, dims_arrow, {}, dim_names_cpx);
    } else {
	return arrow::NumericTensor<TYPE>::Make(buf, dims_arrow, {}, dim_names);
    }
}

}

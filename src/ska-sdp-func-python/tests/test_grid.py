
import pytest

from ska_sdp_func.grid import uvw_grid
import pyarrow, numpy

import itertools
import math
import h5py

DU, DV, DW = 1.5,1,3

@pytest.fixture
def gridder():

    # Read kernel data
    with h5py.File("data/grid/kernel_pswf_8.h5") as f:
        kern = numpy.array(f['sepkern']['kern'])
        corr = numpy.array(f['sepkern']['corr'])

    # Initialise
    yield uvw_grid(DU, DV, DW, kern, corr, kern, corr)

def test_uvw_grid_pybind(gridder):

    # Check that matching step count gets enforced
    grid_size = 10
    grid = numpy.zeros( (grid_size, grid_size, grid_size), dtype=complex)
    channels = 2
    steps = 2
    vis_out = numpy.empty( (steps, channels), dtype=complex)

    # First check that it works correctly if we do everything right
    gridder.degrid(-grid_size//2, -grid_size//2, -grid_size//2,
                   0, False,
                   numpy.zeros( (steps, 6), dtype=float ),
                   numpy.full( (steps, 2), [0, channels], dtype=int),
                   grid, 0, vis_out)

    # Now try it with mismatching step count
    with pytest.raises(RuntimeError, match=r"dimension.*step.*size 2.*expected 1.*uvw_ld"):
        gridder.degrid(-grid_size//2, -grid_size//2, -grid_size//2,
                       0, False,
                       numpy.zeros( (steps-1, 6), dtype=float ),
                       numpy.full( (steps, 2), [0, channels], dtype=int),
                       grid, 0, vis_out)

def test_uvw_grid_all_ones(gridder):
    """ Check that degridding is normalised, i.e. that degridding from an
    all-ones grid gives us visibilities that are exactly one.
    """

    # Allocate grid
    grid_size = 12
    grid = numpy.full( (grid_size, grid_size, grid_size), 1+1j, dtype=complex)
    grid[:,0,:] = 0
    grid[:,:,0] = 0
    grid[:,-1,:] = 0
    grid[:,:,-1] = 0

    # Allocate visibilities
    channels = 4
    uvws = [
        [-DU * channels / 2, 0, 0] + [DU, 0, 0],
        [0, -DV * channels / 2, 0] + [0, DV, 0],
        [DU * (channels / 2-1), 0, 0] + [-DU, 0, 0],
        [0, DV * (channels / 2-1), 0] + [0, -DV, 0]
    ]
    steps = len(uvws)
    vis_out = numpy.empty( (steps, channels), dtype=complex)

    for conj in [False, True]:
        # Degrid visibilities
        gridder.degrid(-grid_size//2, -grid_size//2, -grid_size//2,
                       0, conj,
                       numpy.array(uvws),
                       numpy.array( [0, channels] ),
                       grid, 0, vis_out)

        # Check that exactly the first and last mismatches
        expected = numpy.full((steps, channels), 1+1j, dtype=complex)
        if conj:
            expected = expected.conj()
        expected[0,0] = vis_out[0,0]
        expected[1,0] = vis_out[0,0]
        expected[2,-1] = vis_out[0,0]
        expected[3,-1] = vis_out[0,0]
        assert numpy.all(numpy.abs(vis_out - expected) < 1e-15)



def test_grid_correction_shift(gridder):
    """ Check that degrid correction implements shifts correctly. """

    # Apply grid correction to an image with all values (1-1j)
    grid_size = 12
    image = numpy.full( (grid_size, grid_size), 1-1j, dtype=complex)
    gridder.degrid_correct(-grid_size//2, -grid_size//2,
                           0, 1 / grid_size / DU, 0, 1 / grid_size / DV,
                           image)

    # Center should still be 1-1j (again, normalisation)
    assert numpy.abs(image[grid_size//2, grid_size//2] - (1-1j)) < 1e-14

    # Edge should be very large
    assert numpy.abs(image[0,0] - (1-1j)) > 1e30

    # Check consistency when queried with a shift
    for m, l in itertools.product(range(grid_size), range(grid_size)):
        _l = (l - grid_size // 2) / grid_size / DU
        _m = (m - grid_size // 2) / grid_size / DV

        point = numpy.array(1-1j)
        gridder.degrid_correct(0, 0, _l, 0, _m, 0, point)
        scale = min(numpy.abs(point), numpy.abs(image[m,l]))
        assert numpy.abs(point - image[m,l]) < scale * 1e-13, f"at {_l}/{_m}"
        assert(point.real == -point.imag)

def test_grid_correction_shift_error(gridder):
    """ Check that grid correction objects to querying out of bounds. """

    point = numpy.zeros((), dtype=complex)

    # Check that the error starts appearing exactly as we pass
    # +-0.5/DU or +-0.5/DV
    for l, m in [(-0.5 / DU, 0.5 / DV), (0.5 / DU, -0.5 / DV)]:
        gridder.degrid_correct(0, 0, 0, 0, 0, 0, point)
    for l, m in [(-0.501 / DU, 0.5 / DV), (0.501 / DU, -0.5 / DV)]:
        with pytest.raises(RuntimeError, match=r".* l range .*"):
            gridder.degrid_correct(0, 0, l, 0, m, 0, point)
    for l, m in [(-0.5 / DU, 0.501 / DV), (0.5 / DU, -0.501 / DV)]:
        with pytest.raises(RuntimeError, match=r".* m range .*"):
            gridder.degrid_correct(0, 0, l, 0, m, 0, point)

def test_grid_point_sources(gridder):
    """ Test gridder on grid generated from point sources using DFT. """

    # Set up UVW positions to check: Go into four directions from a
    # number of central points
    channels = 60
    uvws = []
    for x in numpy.arange(-2, 2, 0.3):
        uvws += [ [-2*DU, x*DV, 0   ] + [DU/10, 0, 0],
                  [-2*DU, 0,    x*DW] + [DU/10, 0, 0],
                  [ x*DU,-2*DV, 0   ] + [0, DV/10, 0],
                  [ 0,   -2*DV, x*DW] + [0, DV/10, 0],
                  [ x*DU, 0   ,-2*DW] + [0, 0, DW/10],
                  [ 0,    x*DV,-2*DW] + [0, 0, DW/10] ]
    uvws = numpy.array(uvws)
    steps = uvws.shape[0]
    vis_out = numpy.empty( (steps, channels), dtype=complex)

    # Allocate grid
    grid_size = 16
    grid = numpy.empty( (grid_size, grid_size, grid_size), dtype=complex)

    # Loop through source placements
    for l1, m1 in  [ (0,0), (0.1/DU,0), (0.3/DU,0),
                     (-0.1/DU, 0.3/DV), (-0.25/DU, 0.1/DV), (0.3/DU, 0.1/DV) ]:
        n1 = numpy.sqrt(1 - l1*l1 - m1*m1) - 1

        # Get correction for point
        point = numpy.array(1, dtype=complex)
        gridder.degrid_correct(0, 0, l1, 0, m1, 0, point)

        # Loop through different grid offsets, to make sure we are
        # applying them correctly.
        for odu, odv, odw in itertools.product(range(2), range(2), range(2)):
            grid_u_offset = -grid_size//2+odu
            grid_v_offset = -grid_size//2+odv
            grid_w_offset = -grid_size//2+odw

            # Calculate grid using DFT, with correction applied
            for w, v, u in itertools.product(range(grid_size), range(grid_size), range(grid_size)):
                _u = DU * (u + grid_u_offset)
                _v = DV * (v + grid_v_offset)
                _w = DW * (w + grid_w_offset)
                grid[w, v, u] = point * numpy.exp(2j * numpy.pi * (_u * l1 + _v * m1 + _w * n1))

            # Call degridder on points
            gridder.degrid(grid_u_offset, grid_v_offset, grid_w_offset,
                           0, False, uvws, numpy.full( (steps, 2), [0, channels] ),
                           grid, 0, vis_out)

            # Check results against DFT
            all_expected = numpy.empty_like(vis_out)
            for step, ch in itertools.product(range(steps), range(channels)):
                _u = uvws[step,0] + ch * uvws[step,3]
                _v = uvws[step,1] + ch * uvws[step,4]
                _w = uvws[step,2] + ch * uvws[step,5]
                expected = numpy.exp(2j * numpy.pi * (_u * l1 + _v * m1 + _w * n1))
                all_expected[step,ch]=expected

            numpy.testing.assert_array_almost_equal(vis_out, all_expected, 2)

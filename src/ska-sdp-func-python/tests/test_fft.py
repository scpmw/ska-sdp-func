
import pytest

from ska_sdp_func.fft import wshift_fft
import pyarrow, numpy

import itertools
import math

def test_fft_pybind():

    # Check that we can pass a pyarrow tensor object
    hgt = wdt = 4
    plan_area = numpy.empty((hgt,wdt,2), dtype=float)
    plan_area_arrow = pyarrow.Tensor.from_numpy(plan_area)
    plan = wshift_fft(1,1,1, plan_area_arrow)

    # Check that we cannot pass an immutable tensor, by whatever method
    plan_area.setflags(write=False)
    plan_area_arrow = pyarrow.Tensor.from_numpy(plan_area)
    with pytest.raises(RuntimeError, match=r'.*must be mutable.*'):
        plan = wshift_fft(1,1,1, plan_area_arrow)
    with pytest.raises(RuntimeError, match=r'.*must be mutable.*'):
        plan = wshift_fft(1,1,1, plan_area)

    # Check that extra dimensions do no harm
    plan = wshift_fft(1,1,1, numpy.empty((1, 1, hgt,wdt), dtype=complex) )
    inp = numpy.zeros((hgt,wdt), dtype=complex)
    out = numpy.empty_like(inp)
    plan.fft(0, inp, out)

    # Check that non-complex arrays get refused
    with pytest.raises(RuntimeError, match=r'.*should be complex'):
        plan = wshift_fft(1,1,1, numpy.empty(()) )
    with pytest.raises(RuntimeError, match=r'.*should be complex.*found.*size.*stride'):
        plan = wshift_fft(1,1,1, numpy.empty((hgt,wdt)) )
    with pytest.raises(RuntimeError, match=r'.*should be complex.*found size 3'):
        plan = wshift_fft(1,1,1, numpy.empty((hgt,wdt,3), dtype=float) )

def test_fft_simple():
    """ Test simple known FFT patterns to ensure basic consistency. """

    for wdt, pad, hgt in [(8,0,8), (8,4,16), (16,4,8)]:

        # Create plan and input and output areas, with padded data layout
        plan = wshift_fft(1,1,1, numpy.empty((hgt,wdt+pad), dtype=complex)[:,:wdt] )
        inp = numpy.zeros((hgt,wdt), dtype=complex)
        out = numpy.empty((hgt,wdt+pad), dtype=complex)[:,:wdt]

        # Check FFT of zeroes is zeroes
        plan.fft(0, inp, out)
        assert numpy.all(out == numpy.zeros((hgt,wdt)))

        # Check FFT of a centered one is all ones
        inp[hgt//2,wdt//2] = 1
        plan.fft(0, inp, out)
        assert numpy.all(out == numpy.ones((hgt,wdt)))

        # Check that it works for an imaginary numer
        inp[hgt//2,wdt//2] = -2j
        plan.fft(0, inp, out)
        assert numpy.all(out == numpy.full((hgt,wdt),-2j))

        # Check filled image
        inp[:,:] = 1
        plan.fft(0, inp, out)
        assert out[hgt//2,wdt//2] == wdt*hgt
        out[hgt//2,wdt//2] = 0
        assert numpy.all(out == 0)

        # Check highest frequency, axis distinction
        inp[:,:] = 0
        inp[0,wdt//2] = 1
        inp[hgt//2,0] = 1j
        plan.fft(0, inp, out)
        expected = numpy.tile([[1+1j,1-1j], [-1+1j,-1-1j]],
                              (hgt//2,wdt//2))
        assert numpy.all(out == expected)

        # Check half-frequency
        inp[:,:] = 0
        inp[hgt//2,3*wdt//4] = 1
        inp[3*hgt//4,wdt//2] = 2j
        ref = numpy.array([1, -1j, -1, 1j])
        plan.fft(0, inp, out)
        expected = numpy.tile(ref[None,:] + 2.j * ref[:,None],
                              (hgt//4, wdt//4))
        assert numpy.all(out == expected)

def test_fft_broadcast():

    # Create simple plan
    hgt = wdt = 8
    plan = wshift_fft(1,1,1, numpy.empty((hgt,wdt), dtype=complex) )

    # Now apply it to a series of FFTs
    count = 4
    inp = numpy.zeros((count,hgt,wdt), dtype=complex)
    for i in range(count):
        inp[i,hgt//2,wdt//2] = i
    out = numpy.empty((count,1,hgt,wdt), dtype=complex)
    plan.fft(0, inp, out)

    # Make sure they were all separately executed
    for i in range(count):
        assert numpy.all(out[i] == i)

def test_fft_dft():
    """ Check FFT against a direct Fourier transform evaluation. """

    # Plan for a 8x8x8 cube at resolution 1.1/1/3
    wdt, hgt, planes = 8, 8, 8
    du, dv, dw = 1, 1.1, 3
    plan = wshift_fft(du,dv,dw, numpy.empty((hgt,wdt), dtype=complex) )
    inp = numpy.zeros((hgt,wdt), dtype=complex)
    out = numpy.empty((planes,hgt,wdt), dtype=complex)
    
    for m1, l1 in itertools.product(range(hgt), range(wdt)):

        # Set pixel
        inp[:] = 0
        inp[m1,l1] = 1

        # Do FFT
        w_offset = -planes // 2
        plan.fft(w_offset, inp, out)

        # Check against direct Fourier transform
        _l1 = (l1 - wdt//2) / wdt / du
        _m1 = (m1 - hgt//2) / hgt / dv
        _n1 = math.sqrt(1 - _l1*_l1 - _m1*_m1) - 1
        for w,v,u in itertools.product(range(planes),range(hgt),range(wdt)):
            _u = du * (u - wdt//2)
            _v = dv * (v - hgt//2)
            _w = dw * (w + w_offset)
            ph = -2j * math.pi * (_u*_l1 + _v*_m1 + _w*_n1)
            
            assert numpy.abs(out[w,v,u] - numpy.exp(ph)) < 1e-14


#include <pybind11/pybind11.h>

#include <ska-sdp-func/fft/wshift_fft.h>
#include <ska-sdp-func/grid/uvw_grid.h>

#include <ska-sdp-func-python/conversions.h>
#include <ska-sdp-func-python/docstrings.h>

namespace py = pybind11;

PYBIND11_MODULE(ska_sdp_func, m) {

    // Import numpy multiarray + pyarrow bits
    import_array1();
    arrow::py::import_pyarrow();

    auto m_fft = m.def_submodule("fft", "Fast Fourier Transforms");
    auto m_grid = m.def_submodule("grid", "De/gridding");

    using ska_sdp_func::tensor_ptr;
    py::class_<ska_sdp_func::wshift_fft>(m_fft, "wshift_fft",
					 DOC(ska_sdp_func, wshift_fft))
	.def(py::init<double, double, double, const tensor_ptr<double> >(),
	     py::arg("du"), py::arg("dv"), py::arg("dw"),
	     py::arg("plan_area"),
	     DOC(ska_sdp_func, wshift_fft, wshift_fft))
	.def("fft", &ska_sdp_func::wshift_fft::fft,
	     py::call_guard<py::gil_scoped_release>(),
	     py::arg("wstep"), py::arg("image"), py::arg("grid_out"),
	     DOC(ska_sdp_func, wshift_fft, fft));

    py::class_<ska_sdp_func::uvw_grid>(m_grid, "uvw_grid", DOC(ska_sdp_func, uvw_grid))
	.def(py::init<double, double, double,
		      tensor_ptr<double>, tensor_ptr<double>,
		      tensor_ptr<double>, tensor_ptr<double> >(),
	     py::arg("du"), py::arg("dv"), py::arg("dw"),
	     py::arg("kernel"), py::arg("corr"),
	     py::arg("wkernel"), py::arg("wcorr"),
	     DOC(ska_sdp_func, uvw_grid, uvw_grid))
	.def("degrid_correct",
	     &ska_sdp_func::uvw_grid::degrid_correct,
	     py::call_guard<py::gil_scoped_release>(),
	     py::arg("il0"), py::arg("im0"),
	     py::arg("l_mid"), py::arg("dl"),
	     py::arg("m_mid"), py::arg("dm"),
	     py::arg("image_inout"),
	     DOC(ska_sdp_func, uvw_grid, degrid_correct))
	.def("degrid", &ska_sdp_func::uvw_grid::degrid,
	     py::call_guard<py::gil_scoped_release>(),
	     py::arg("u0"), py::arg("v0"), py::arg("w0"),
	     py::arg("ch0"), py::arg("conjugate"),
	     py::arg("uvw_ld"), py::arg("channels"),
	     py::arg("grid"), py::arg("grid_w_roll"),
	     py::arg("vis_out"),
	     DOC(ska_sdp_func, uvw_grid, degrid));

}


C++ API
=======

Gridding
--------

.. doxygenclass:: ska_sdp_func::uvw_grid
   :members:
   :undoc-members:

Fourier Transformation
----------------------

.. doxygenclass:: ska_sdp_func::wshift_fft
   :members:
   :undoc-members:


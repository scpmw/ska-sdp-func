

# Get libraries
execute_process(COMMAND ${Python3_EXECUTABLE} -c "import pyarrow; print(' '.join(pyarrow.get_libraries()), end='')"
  OUTPUT_VARIABLE PYARROW_LIBRARIES_RAW)
separate_arguments(PYARROW_LIBRARIES_RAW)
execute_process(COMMAND ${Python3_EXECUTABLE} -c "import pyarrow; print(' '.join(pyarrow.get_library_dirs()), end='')"
  OUTPUT_VARIABLE PYARROW_LIBRARY_DIRS)
separate_arguments(PYARROW_LIBRARY_DIRS)
execute_process(COMMAND ${Python3_EXECUTABLE} -c "import pyarrow; print(pyarrow.get_include(), end='')"
  OUTPUT_VARIABLE PYARROW_INCLUDE_DIRS)

# Check whether libraries actually exist at the place specified by
# pyarrow, i.e. we are actually linking against libraries embedded
# in a Python wheel.
unset(ARROW_LIBRARIES)
unset(PYARROW_LIBRARIES)
foreach(lib IN ITEMS ${PYARROW_LIBRARIES_RAW})
  foreach(lib_dir IN ITEMS ${PYARROW_LIBRARY_DIRS})
    if(EXISTS "${lib_dir}/lib${lib}.so")
      set(found "${lib_dir}/lib${lib}.so")
      break()
    else()
      file(GLOB lib_version "${lib_dir}/lib${lib}.so.*")
      if (NOT lib_version STREQUAL "")
        list(POP_BACK lib_version found)
        break()
      endif()
    endif()
  endforeach()
  if(lib STREQUAL "arrow")
    list(APPEND ARROW_LIBRARIES ${found})
  else()
    list(APPEND PYARROW_LIBRARIES ${found})
  endif()
endforeach()


if(DEFINED ARROW_LIBRARIES)
  # We are using the wheel - in this case. We need to *not* use C++11
  # ABI to retain a chance to link successfully
  include_directories(${PYARROW_INCLUDE_DIRS})
  add_definitions(-D_GLIBCXX_USE_CXX11_ABI=0)

  message(WARNING "We are about to link against Arrow libraries bundled with PyArrow,\
  which requires -D_GLIBCXX_USE_CXX11_ABI=0. This works globally, so\
  if any other library uses the C++11 ABI, this will cause problems! \
  It is generally recommended to install pyarrow so it uses a system-provided\
  library. Check the README for details.")

endif()

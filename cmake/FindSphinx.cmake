find_program(SPHINX_EXEC NAMES sphinx-build DOC "sphinx-build path")
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Sphinx "Did not find sphinx-build" SPHINX_EXEC)
